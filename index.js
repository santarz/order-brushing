const fs = require('fs')
const parse = require('csv-parse')
const _ = require('lodash')
const momemt = require('moment')
const createCsvWriter = require('csv-writer').createObjectCsvWriter

const csvToJson = (csv) => {
  const [firstLine, ...lines] = csv
  return lines.map((line) =>
    firstLine.reduce(
      (curr, next, index) => ({
        ...curr,
        [next]: line[index],
      }),
      {},
    ),
  )
}

const csvWriter = createCsvWriter({
  path: './order_brush_order_output.csv',
  header: [
    { id: 'shopid', title: 'shopid' },
    { id: 'userid', title: 'userid' },
  ],
})

const groupFunc = (data) => {
  const { shopid, event_time } = data
  return `${shopid}-${momemt(event_time, 'YYYY-MM-DD HH:mm:ss').format(
    'YYYY/MM/DD/HH',
  )}`
}

const filterFunc = (data) => {
  const reduced = Object.keys(data).reduce((acc, cur) => {
    const value = data[cur]
    const uniq = _.uniq(value.map((e) => e.userid))
    const rate = value.length / uniq.length
    if (rate >= 3) {
      if (acc.hasOwnProperty(cur.split('-')[0])) {
        acc[cur.split('-')[0]].push(uniq)
      } else {
        acc[cur.split('-')[0]] = uniq
      }
    }
    return acc
  }, {})
  return reduced
}

const data = []
fs.createReadStream('./order_brush_order.csv')
  .pipe(parse({ delimiter: ',' }))
  .on('data', (r) => {
    data.push(r)
  })
  .on('end', () => {
    const json = csvToJson(data)
    const groupData = _.groupBy(json, groupFunc)
    const filtered = filterFunc(groupData)
    const uniqShop = _.uniq(json.map((e) => e.shopid))
    const result = uniqShop.map((e) => {
      const userid = _.uniq(filtered[e])
      return {
        shopid: e,
        userid: userid.length === 0 ? 0 : userid.join('&'),
      }
    })
    csvWriter
      .writeRecords(result)
      .then(() => console.log('The CSV file was written successfully'))
  })
